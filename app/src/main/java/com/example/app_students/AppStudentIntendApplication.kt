package com.example.app_students

import android.app.Application
import android.content.Context
import com.example.app_students.repository.StudentDBRepository
import com.example.app_students.repository.StudentRepository

class AppStudentIntendApplication : Application() {

    init {
        instance = this
    }

    companion object {
        private var instance: AppStudentIntendApplication? = null
        fun applicationContext() : Context {
            return instance!!.applicationContext
        }
    }

    override fun onCreate() {
        super.onCreate()
        StudentDBRepository.initialize(this)
    }

}