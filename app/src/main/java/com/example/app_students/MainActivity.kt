package com.example.app_students

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.PersistableBundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.activity.OnBackPressedCallback
import androidx.appcompat.app.AlertDialog
import com.example.app_students.MyConstants.STUDENT_INFO_FRAGMENT_TAG
import com.example.app_students.MyConstants.STUDENT_LIST_FRAGMENT_TAG
import com.example.app_students.data.Student
import com.example.app_students.repository.StudentRepository
import java.util.*

class MainActivity : AppCompatActivity(), StudentListDBFragment.Callbacks, StudentInfoDBFragment.Callbacks {

    private var miAdd : MenuItem? = null
    private var miDelete : MenuItem? = null
    private var miChange : MenuItem? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //showStudentsList()
        showDBStudents()

        val callback = object : OnBackPressedCallback(true)
            {
                override fun handleOnBackPressed() {
                    checkLogout()
                }
            }

        onBackPressedDispatcher.addCallback(this, callback)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        saveData()
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(
        savedInstanceState: Bundle?,
        persistentState: PersistableBundle?
    ) {
        loadData()
        super.onRestoreInstanceState(savedInstanceState, persistentState)
    }

    private fun loadData() {
        StudentRepository.getInstance().loadStudents()
    }

    private fun saveData() {
        StudentRepository.getInstance().saveStudents()
    }

    override fun onStop() {
        saveData()
        super.onStop()
    }

    private fun checkLogout() {
        AlertDialog.Builder(this)
            .setTitle("Выход!")
            .setMessage("Вы действительно хотите выйти из приложения?")

            .setPositiveButton("ДА") { _, _ ->
                finish()
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }

    fun checkDelete() {
        val s = StudentRepository.getInstance().student.value?.lastName+" "+
                StudentRepository.getInstance().student.value?.firstName+" "+
                StudentRepository.getInstance().student.value?.middleName
        AlertDialog.Builder(this)
            .setTitle("УДАЛЕНИЕ!")
            .setMessage("Вы действительно хотите удалить студента $s ?")
            .setPositiveButton("ДА") { _, _ ->
                StudentRepository.getInstance().deleteStudentCurrent()
            }
            .setNegativeButton("НЕТ", null)
            .setCancelable(true)
            .create()
            .show()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.main_menu, menu)
        miAdd=menu.findItem(R.id.miAdd)
        miDelete=menu.findItem(R.id.miDelete)
        miChange=menu.findItem(R.id.miChange)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.miAdd -> {
                //showNewStudent()
                showStudentDetailDB(Student().id)
                true
            }
            R.id.miDelete -> {
                checkDelete()
                true
            }
            R.id.miChange -> {
                showStudentInfo()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun showNewStudent() {
        StudentRepository.getInstance().newStudent()
        showStudentInfo()
    }

    fun showStudentsList() {
        miAdd?.isVisible=true
        miDelete?.isVisible=true
        miChange?.isVisible=true
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, StudentsListFragment.getInstance(), STUDENT_LIST_FRAGMENT_TAG)
            .commit()
    }

    fun showStudentInfo() {
        miAdd?.isVisible=false
        miDelete?.isVisible=false
        miChange?.isVisible=false
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, StudentInfoFragment.getInstance(), STUDENT_INFO_FRAGMENT_TAG)
            //.addToBackStack(null)
            .commit()
    }

    override fun onStudentSelected(studentId: UUID) {
        showStudentDetailDB(studentId)
    }

    private fun showStudentDetailDB(studentId: UUID) {
        val fragment = StudentInfoDBFragment.newInstance(studentId)
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, fragment)
            .commit()
    }

    override fun showDBStudents() {
        supportFragmentManager
            .beginTransaction()
            .replace(R.id.frame, StudentListDBFragment.getInstance(), STUDENT_LIST_FRAGMENT_TAG)
            .commit()
    }

}