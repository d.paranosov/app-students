package com.example.app_students

object MyConstants {
    const val STUDENT_LIST_FRAGMENT_TAG = "com.example.app_students.student_list"
    const val STUDENT_INFO_FRAGMENT_TAG = "com.example.app_students.student_info"
    const val TAG = "com.example.app_students.log_tag"
    const val STUDENT_ID_TAG = "com.example.app_students.student_id"
}