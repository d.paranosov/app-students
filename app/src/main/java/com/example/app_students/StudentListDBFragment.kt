package com.example.app_students

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_students.data.Student
import java.util.*

class StudentListDBFragment : Fragment() {

    private lateinit var studentsListViewModel: StudentListDBViewModel
    private lateinit var studentsListRecyclerView: RecyclerView

    private var adapter: StudentListAdapter? = StudentListAdapter(emptyList())

    companion object {
        private var INSTANCE : StudentListDBFragment? = null

        fun getInstance(): StudentListDBFragment {
            if (INSTANCE == null) {
                INSTANCE = StudentListDBFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение списка не создано!")
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutView = inflater.inflate(R.layout.list_of_students, container, false)
        studentsListRecyclerView = layoutView.findViewById(R.id.rvList)
        studentsListRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        studentsListRecyclerView.adapter = adapter
        return layoutView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        studentsListViewModel = ViewModelProvider(this).get(StudentListDBViewModel::class.java)
        studentsListViewModel.studentListLiveData.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer { students ->
                students?.let {
                    updateUI(students)
                }
            })
    }

    private inner class StudentListAdapter(private val items: List<Student>)
        : RecyclerView.Adapter<StudentHolder>() {
        override fun onCreateViewHolder(
            parrent : ViewGroup,
            viewType :Int
        ):StudentHolder{
            val view = layoutInflater.inflate(R.layout.students_list_element, parrent, false)
            return StudentHolder(view)
        }

        override fun getItemCount(): Int = items.size

        override fun onBindViewHolder(holder: StudentHolder, position: Int) {
            holder.bind(items[position])
        }
    }

    private inner class StudentHolder(view : View)
        : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener{
        private lateinit var student: Student
        private val fioTextView : TextView = itemView.findViewById(R.id.tvFIO)
        private val ageTextView : TextView = itemView.findViewById(R.id.tvAge)
        private val groupTextView : TextView = itemView.findViewById(R.id.tvGroup)
        private val clLayout: ConstraintLayout = itemView.findViewById(R.id.clCL)

        fun bind(student: Student){
            //Log.d(MyConstants.TAG, "bind 1 $student")
            this.student = student
            fioTextView.text = "${student.lastName} ${student.firstName} ${student.middleName}"
            groupTextView.text = student.group
            ageTextView.text = student.age.toString()
            //Log.d(MyConstants.TAG, "bind 2 $student")
        }

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            callbacks?.onStudentSelected(student.id)
        }

        override fun onLongClick(v: View?): Boolean {

            return true
        }

    }

    private fun updateUI(students: List<Student>) {
        if (students==null) return
        adapter=StudentListAdapter(students)
        studentsListRecyclerView.adapter = adapter
    }

    interface Callbacks {
        fun onStudentSelected(studentId: UUID)
    }

    private var callbacks: Callbacks? = null
    override fun onAttach(context: Context) {
        super.onAttach(context)
        callbacks = context as Callbacks?
    }

    override fun onDetach() {
        super.onDetach()
        callbacks = null
    }

}