package com.example.app_students

import androidx.lifecycle.ViewModel
import com.example.app_students.repository.StudentDBRepository

class StudentListDBViewModel : ViewModel() {
    private val studentRepository = StudentDBRepository.get()
    val studentListLiveData = studentRepository.getStudents()
}