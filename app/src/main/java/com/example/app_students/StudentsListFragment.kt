package com.example.app_students

import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.app_students.data.Student
import com.example.app_students.data.StudentsList

class StudentsListFragment : Fragment() {
    private lateinit var studentsListViewModel: StudentsListViewModel
    private lateinit var studentsListRecyclerView: RecyclerView

    companion object {
        private var INSTANCE : StudentsListFragment? = null

        fun getInstance(): StudentsListFragment {
            if (INSTANCE == null) {
                INSTANCE = StudentsListFragment()
            }
            return INSTANCE?: throw IllegalStateException("Отображение списка не создано!")
        }
    }

    private lateinit var viewModel: StudentsListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val layoutView = inflater.inflate(R.layout.list_of_students, container, false)
        studentsListRecyclerView = layoutView.findViewById(R.id.rvList)
        studentsListRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        return layoutView
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        studentsListViewModel = ViewModelProvider(this).get(StudentsListViewModel::class.java)
        studentsListViewModel.studentsList.observe(viewLifecycleOwner){
            updateUI(it)
        }
    }

    private inner class StudentListAdapter(private val orderItems: List<Student>) : RecyclerView.Adapter<StudentHolder>()
    {
        override fun onCreateViewHolder(
            parrent : ViewGroup,
            viewType :Int
        ):StudentHolder{
            val view = layoutInflater.inflate(R.layout.students_list_element, parrent, false)
            return StudentHolder(view)
        }

        override fun getItemCount(): Int = orderItems.size

        override fun onBindViewHolder(holder: StudentHolder, position: Int) {
            holder.bind(orderItems[position])
        }
    }

    private inner class StudentHolder(view :View)
        : RecyclerView.ViewHolder(view), View.OnClickListener, View.OnLongClickListener{
        private lateinit var student: Student
        private val fioTextView : TextView = itemView.findViewById(R.id.tvFIO)
        private val ageTextView : TextView = itemView.findViewById(R.id.tvAge)
        private val groupTextView : TextView = itemView.findViewById(R.id.tvGroup)
        private val clLayout: ConstraintLayout = itemView.findViewById(R.id.clCL)

        fun bind(student: Student){
            Log.d(MyConstants.TAG, "bind 1 $student")
            this.student = student
            clLayout.setBackgroundColor(context!!.getColor(R.color.white))
            if (student.id == studentsListViewModel.student.id)
                clLayout.setBackgroundColor(context!!.getColor(R.color.element))
            fioTextView.text = "${student.lastName} ${student.firstName} ${student.middleName}"
            groupTextView.text = student.group
            ageTextView.text = student.age.toString()
            Log.d(MyConstants.TAG, "bind 2 $student")
        }

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        override fun onClick(v: View?) {
            studentsListViewModel.setStudent(student)
            (requireActivity() as MainActivity).showStudentInfo()
            //updateUI(studentsListViewModel.studentsList.value, studentsListRecyclerView.verticalScrollbarPosition)
            /*val p = studentsListRecyclerView.verticalScrollbarPosition
            studentsListRecyclerView.adapter = StudentListAdapter(studentsListViewModel.studentsList.value!!.items)
            studentsListRecyclerView.verticalScrollbarPosition = p*/
        }

        override fun onLongClick(v: View?): Boolean {
            studentsListViewModel.setStudent(student)
            (requireActivity() as MainActivity).checkDelete()
            return true
        }

    }

    private fun updateUI(studentsList: StudentsList?=null) {
        if (studentsList==null) return
        studentsListRecyclerView.adapter = StudentListAdapter(studentsList.items)
        //studentsListRecyclerView.verticalScrollbarPosition = position
        //studentsListRecyclerView.layoutManager?.scrollToPosition(position)
    }

}