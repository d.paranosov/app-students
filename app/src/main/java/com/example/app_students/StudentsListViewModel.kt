package com.example.app_students

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import com.example.app_students.data.StudentsList
import com.example.app_students.MyConstants.TAG
import com.example.app_students.data.Student
import com.example.app_students.repository.StudentRepository

class StudentsListViewModel : ViewModel() {
    var studentsList : MutableLiveData<StudentsList> = MutableLiveData()

    var studentCurrent : Student = Student()
    val student : Student
        get() = studentCurrent

    private var observer : Observer<StudentsList> = Observer<StudentsList>
    {
        newList ->
            newList?.let {
            Log.d(TAG,"Получен список StudentsListViewModel от StudentRepository")
            studentsList.postValue(newList)
        }
    }

    init {
        StudentRepository.getInstance().student.observeForever {
            studentCurrent = it
            Log.d(TAG, "Получили Student в StudentInfoViewModel")
        }
        StudentRepository.getInstance().studentsList.observeForever(observer)
        Log.d(TAG, "Подписались StudentsListViewModel к StudentRepository")
    }

    fun setStudent(student: Student) {
        StudentRepository.getInstance().setCurrentStudent(student)
    }
}