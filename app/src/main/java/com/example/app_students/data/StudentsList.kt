package com.example.app_students.data

data class StudentsList(
    val items : MutableList<Student> = mutableListOf()
)
