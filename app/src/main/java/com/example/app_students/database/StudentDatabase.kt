package com.example.app_students.database

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.app_students.data.Student

@Database(entities = [ Student::class ], version = 1)
@TypeConverters(StudentTypeConverters::class)
abstract class StudentDatabase : RoomDatabase() {
    abstract fun studentDao(): StudentDao
}