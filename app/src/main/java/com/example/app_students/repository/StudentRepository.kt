package com.example.app_students.repository

import androidx.lifecycle.MutableLiveData
import androidx.preference.Preference
import androidx.preference.PreferenceManager
import com.example.app_students.AppStudentIntendApplication
import com.example.app_students.StudentInfoFragment
import com.example.app_students.data.Student
import com.example.app_students.data.StudentsList
import com.google.gson.Gson

class StudentRepository {

    companion object {
        private var INSTANCE : StudentRepository?=null

        fun getInstance(): StudentRepository {
            if (INSTANCE == null) {
                INSTANCE = StudentRepository()
            }
            return INSTANCE?:
            throw IllegalStateException("Репозиторий StudentsRepository не инициализирован")
        }
    }

    init {
        loadStudents()
    }

    var studentsList: MutableLiveData<StudentsList> = MutableLiveData()
    var student:MutableLiveData<Student> = MutableLiveData()

    fun loadStudents() {
        val jsonString =
            PreferenceManager.getDefaultSharedPreferences(AppStudentIntendApplication.applicationContext())
                .getString("students", null)
        if (!jsonString.isNullOrBlank()) {
            val st = Gson().fromJson(jsonString, StudentsList::class.java)
            if (st != null)
                this.studentsList.postValue(st)
        }
    }

    fun saveStudents() {
        val gson = Gson()
        val jsonStudents = gson.toJson(studentsList.value)
        val preference =
            PreferenceManager.getDefaultSharedPreferences(AppStudentIntendApplication.applicationContext())
        preference.edit().apply {
            putString("students", jsonStudents)
            apply()
        }
    }

    fun setCurrentStudent(position: Int) {
        if (studentsList.value == null || studentsList.value!!.items == null ||
            position < 0 || (studentsList.value?.items?.size!! <= position))
            return
        student.postValue(studentsList.value?.items!![position])
    }

    fun setCurrentStudent(student: Student) {
        this.student.postValue(student)
    }

    fun addStudent(student: Student) {
        var studentsListTmp = studentsList
        if (studentsListTmp.value == null)
            studentsListTmp.value = StudentsList()
        studentsListTmp.value!!.items.add(student)
        studentsList.postValue(studentsListTmp.value)
    }

    fun getPosition(student: Student) : Int = studentsList.value?.items?.indexOfFirst {
        it.id == student.id } ?: -1

    fun updateStudent(student: Student) {
        var studentsListTmp = studentsList
        val position = getPosition(student)
        if (studentsListTmp.value == null || position < 0)
            addStudent(student)
        else {
            studentsListTmp.value!!.items[position] = student
            studentsList.postValue(studentsListTmp.value)
        }
    }
    fun deleteStudent(student: Student) {
        var studentsListTmp = studentsList
        if (studentsListTmp.value!!.items.remove(student)) {
            studentsList.postValue(studentsListTmp.value)
        }
    }

    fun deleteStudentCurrent() {
        if (student.value!=null)
            deleteStudent(student.value!!)
    }

    fun newStudent() {
        student.postValue(Student())
    }

}